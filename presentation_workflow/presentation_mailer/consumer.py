import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(ch, method, properties, body):
    print(" [x] Received %r" % body)
    content = json.loads(body)
    name = content["presenter_name"]
    title = content["title"]
    send_mail(
        "Your presentation has been accepted",
        f"{name}, we're happy to tell you that your presentation {title} has been accepted",
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )


def process_rejection(ch, method, properties, body):
    print(" [x] Received %r" % body)
    content = json.loads(body)
    name = content["presenter_name"]
    title = content["title"]
    send_mail(
        "Your presentation has been rejected",
        f"{name}, we're sorry to tell you that your presentation {title} has been rejected",
        "admin@conference.go",
        [content["presenter_email"]],
        fail_silently=False,
    )


def main():
    while True:
        try:
            parameters = pika.ConnectionParameters(host="rabbitmq")
            connection = pika.BlockingConnection(parameters)
            channel = connection.channel()
            channel.queue_declare(queue="presentation_approvals")
            channel.basic_consume(
                queue="presentation_approvals",
                on_message_callback=process_approval,
                auto_ack=True,
            )
            channel.queue_declare(queue="presentation_rejections")
            channel.basic_consume(
                queue="presentation_rejections",
                on_message_callback=process_rejection,
                auto_ack=True,
            )

            # Print a status
            print(" [*] Waiting for messages. To exit press CTRL+C")

            # Tell RabbitMQ that you're ready to receive messages
            channel.start_consuming()
        except AMQPConnectionError:
            print("Could not connect to RabbitMQ")
            time.sleep(2.0)


# Just extra stuff to do when the script runs
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print("Interrupted")
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)
